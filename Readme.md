CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * MODULE FEATURES
 * Configuration
 * KNOWN COMPATIBILITY ISSUES
 * Maintainers


INTRODUCTION
------------
Provides a purge queuer and a service which can be used to queue everything invalidation.

REQUIREMENTS
------------

Purge module. See: https://www.drupal.org/project/purge.


INSTALLATION
------------

Refer the Purge documentation to get more details on how to setup the various
purge modules and queuers.
https://git.drupalcode.org/project/purge/-/blob/8.x-3.x/README.md

MODULE FEATURES
---------------
 * Provides a purge queuer named Everything queuer which allows queuing of everything invalidation.

 * Provides a Drupal service named `purge_everything_queuer.everything` which can
   be used to queue everything invalidation. When used, by default it clears all
   existing items in the queue.

 * Usage example:
 ```
 \Drupal::service('purge_everything_queuer.everything')->queueEverything();
 ```

 * Provides a cron job for cleaning out the purge queue if it is having more
  than 100,000 items, so that purge process is not halted.

CONFIGURATION
-------------

 * Refer https://git.drupalcode.org/project/purge/-/blob/8.x-3.x/README.md for
   setting up purge queuers.

KNOWN COMPATIBILITY ISSUES
--------------------------
* Purge Queues (https://www.drupal.org/project/purge_queues): To work correctly with this module this patch (https://www.drupal.org/project/purge_queues/issues/3201128#comment-14016946) needs to be applied.

MAINTAINERS
-----------

Current maintainers:
 * Hemant Gupta (https://www.drupal.org/u/guptahemant)
 * Graham Arrowsmith (https://www.drupal.org/u/arrowsmith)
