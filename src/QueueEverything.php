<?php

namespace Drupal\purge_everything_queuer;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\purge\Plugin\Purge\Invalidation\Exception\TypeUnsupportedException;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface;
use Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface;
use Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface;

/**
 * Helper service to queue everything invalidation.
 */
class QueueEverything {

  /**
   * The 'purge.invalidation.factory' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface
   */
  protected $purgeInvalidationFactory;

  /**
   * The 'purge.queue' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface
   */
  protected $purgeQueue;

  /**
   * The 'purge.queuers' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface
   */
  protected $purgeQueuers;

  /**
   * Constructs a QueueEverything object.
   *
   * @param Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface $purgeInvalidationFactory
   *   The 'purge.invalidation.factory' service.
   * @param \Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface $purgeQueue
   *   The 'purge.queue' service.
   * @param \Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface $purgeQueuers
   *   The 'purge.queuers' service.
   */
  public function __construct(InvalidationsServiceInterface $purgeInvalidationFactory, QueueServiceInterface $purgeQueue, QueuersServiceInterface $purgeQueuers) {
    $this->purgeInvalidationFactory = $purgeInvalidationFactory;
    $this->purgeQueue = $purgeQueue;
    $this->purgeQueuers = $purgeQueuers;
  }

  /**
   * Clears the current queue and queues everything invalidation object.
   *
   * @param bool $clear_queue
   *   If set to true the current invalidation objects will be removed.
   */
  public function queueEverything(bool $clear_queue = TRUE) {
    $queuer = $this->purgeQueuers->get('everything');
    if ($queuer !== FALSE) {
      $invalidations = [];
      try {
        $invalidations[] = $this->purgeInvalidationFactory->get('everything', NULL);
      }
      catch (TypeUnsupportedException $e) {
        // When there's no purger enabled for it, don't bother queuing.
        return;
      }
      catch (PluginNotFoundException $e) {
        // When Drupal uninstalls Purge, rebuilds plugin caches it might
        // run into the condition where the everything plugin isn't available.
        // In these scenarios we want the queuer to silently fail.
        return;
      }

      if (count($invalidations)) {
        if ($clear_queue) {
          // Clear the queue.
          $this->purgeQueue->emptyQueue();
        }

        // Add invalidations in the queue.
        $this->purgeQueue->add($queuer, $invalidations);
      }
    }
  }

}
