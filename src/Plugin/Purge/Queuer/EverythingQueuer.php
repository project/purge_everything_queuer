<?php

namespace Drupal\purge_everything_queuer\Plugin\Purge\Queuer;

use Drupal\purge\Plugin\Purge\Queuer\QueuerBase;
use Drupal\purge\Plugin\Purge\Queuer\QueuerInterface;

/**
 * Queues everything invalidation.
 *
 * @PurgeQueuer(
 *   id = "everything",
 *   label = @Translation("Everything queuer"),
 *   description = @Translation("Queues everything invalidation."),
 *   enable_by_default = true,
 *   configform = "",
 * )
 */
class EverythingQueuer extends QueuerBase implements QueuerInterface {

}
